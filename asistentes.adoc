== ﻿Lista de asistentes

// Ordenados por orden alfabético
// Formato: Apellidos, Nombre

* Benítez Galván, Salvador
* Casermeiro Pérez, Paula
* Castro Navarrete, Francisco Javier
* Cid Medina, Rafael
* Contreras Fernández, Julio
* Díaz Ventura, Francisco Javier
* Godino Guerra, Nuria
* López Galisteo, Samuel
* Luque Giráldez, José Rafael
* Manzano Montenero, José Julián
* Martínez Morales, Raúl
* Núñez Marín, Álvaro
* Zea Diez, Jaime

=== Organización del transporte

// Si tienes vehículo, pon el número de plazas. Si no tienes, añádete a
// alguno de los vehículos existentes.

=== Fiat Multipla (5 plazas)

* Salvador Benitez
* Samuel López
* Raúl Martínez

==== Peugeot 508 (5 plazas)

* Rafael Luque
* Castro Navarrete, Francisco Javier

==== VolksWagen Passat (5 plazas)

* Alvaro Nuñez
* Julio Contreras
* Rafael Cid
* Nuria Godino

==== Audi A4 (5 plazas)

* Paula Casermeiro
* Jaime Zea Diez
* Francisco Javier Díaz Ventura
* José Julián Manzano Montenegro
